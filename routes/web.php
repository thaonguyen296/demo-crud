<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\CourseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::get('/add-post',[PostController::class,'addPost']);
//Route::post('/create-post',[PostController::class,'createPost'])->name('post.create');
//
//Route::get('/posts', [PostController::class, 'index']);
//Route::get('/posts/{id}', [PostController::class, 'getPostById']);
//
//Route::get('/edit-post/{id}', [PostController::class,'editPost']);
//Route::post('/update-post', [PostController::class,'updatePost'])->name('post.update');
//
//Route::get('/delete-post/{id}',[PostController::class,'deletePost']);


//Route Course
Route::get('/course/add-course', [CourseController::class, 'addCourse']);
Route::post('/course/create-course',[CourseController::class, 'createCourse'])->name('course.create');

Route::get('/course/list-course', [CourseController::class,'getCourse']);

Route::get('/course/edit-course/{id}',[CourseController::class,'editCourse']);
Route::post('/course/update-course',[CourseController::class,'updateCourse'])->name('course.update');

Route::get('/course/delete-course/{id}',[CourseController::class,'deleteCourse']);

//Route Student
Route::get('/student/add-student',[StudentController::class,'addStudent']);
Route::post('/student/create-student',[StudentController::class,'createStudent'])->name('student.create');

Route::get('/student/list-student',[StudentController::class,'getStudent']);

Route::get('/student/edit-student/{id}',[StudentController::class,'editStudent']);
Route::post('/student/update-student',[StudentController::class,'updateStudent'])->name('student.update');

Route::get('/student/delete-student/{id}',[StudentController::class,'deleteStudent']);


//Route Post
Route::get('/post', [PostController::class, 'index'])->name('post');
Route::post('/post/savePost',[PostController::class, 'savePost'])->name('savePost');
Route::get('/editPost/{id}',[PostController::class, 'edit'])->name('editPost');
Route::post('updatePost',[PostController::class, 'update'])->name('updatePost');
Route::get('/deletePost/{id}',[PostController::class, 'delete'])->name('deletePost');

