<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Post;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function addCourse(){
        return view('/course/add-course');
    }

    public function createCourse(Request $request){
        $course = new Course();
        $course->course_name = $request->course_name;
        $course->price = $request->price;
        $course->save();
        return redirect('/course/list-course')->with('course_created', 'Course has been created successfully!');
    }
    public function getCourse(){
        $courses = Course::orderBy('course_name','price')->get();
        return view('/course/list-course',compact('courses'));
    }
    public function editCourse($id){
        $course = Course::find($id);
        return view('/course/edit-course',compact('course'));
    }
    public function updateCourse(Request $request){
        $course = Course::find($request->id);
        $course->course_name = $request->course_name;
        $course->price = $request->price;
        $course->save();
        return redirect('/course/list-course');
    }

    public function deleteCourse($id){
        Course::find($id)->delete();
        return back()->with('course_deleted','Course has been deleted successfully!');
    }
}
