<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function addStudent(){
        return view('/student/add-student');
    }

    public function createStudent(Request $request){
        $student = new Student();
        $student->studname = $request->studname;
        $student->gender = $request->gender;
        $student->age = $request->age;
        $student->save();
        return redirect('/student/list-student');
    }

    public function getStudent(){
        $students = Student::orderBy('studname','gender','age')->get();
        return view('/student/list-student',compact('students'));
    }

    public function editStudent($id){
        $student = Student::find($id);
        return view('/student/edit-student',compact('student'));
    }
    public function updateStudent(Request $request){
        $student = Student::find($request->id);
        $student->studname = $request->studname;
        $student->gender = $request->gender;
        $student->age = $request->age;
        $student->save();
        return redirect('/student/list-student');
    }

    public function deleteStudent($id)
    {
        Student::find($id)->delete();
        return back()->with('/student_deleted','Student has been deleted successfully!');
    }
}
