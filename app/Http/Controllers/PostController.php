<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Services\PostService;

class PostController extends Controller
{

    private $postService;
    /**
     * ThankController constructor.
     * @param $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function index(){
        $post = $this->postService->getAllPost();
        return view('posts',['posts'=>$post]);
    }

    public function savePost(Request $request){
        $this->postService->insertPost($request);
        return redirect('/posts');
    }

    public function edit($id){
        $post = $this->postService->findPost($id);
        return view('edit-post', ['singlePost'=>$post]);
    }

    public function update( Request $request)
    {
        $this->postService->savePost($request);
        return redirect('/posts');
    }
    public function delete($id)
    {
        $post = $this->postService->Delete($id);
        return redirect('/posts');
    }








//    public function addPost(){
//        return view('add-post');
//    }
//
//    public function createPost(Request $request){
////        $request->validate([
////            'title' => 'bail|required|unique:posts|max:255',
////            'body'=>'required',
////    ]);
////        $post = new Post();
////        $post->title = $request->title;
////        $post->body = $request->body;
////        $post->save();
//        return redirect('/posts')->with('post_created','Post has been created successful');
//    }
//    public function getPost(){
////        $posts = Post::orderBy('id','DESC')->get();
//        return view('posts',compact('posts'));
//    }
////    public function getPostById($id){
////        $post = Post::where('id',$id)->first();
////        return view('single-post',compact('post'));
////    }
//    public function editPost($id){
//        $post = Post::find($id);
//        return view('edit-post', compact('post'));
//    }
//    public function updatePost(Request $request){
//        $post = Post::find($request->id);
//        $post->title = $request->title;
//        $post->body = $request->body;
//        $post->save();
//        return redirect('/posts')->with('post_updated','Post has been updated successfully');
//    }
//
//    public function deletePost($id){
//        Post::find($id)->delete();
//        return back()->with('post_deleted','Post has been deleted successfully!');
//    }

}
