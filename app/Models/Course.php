<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Course extends Eloquent
{
//    use HybridRelations;

    protected $connection = 'mongodb';
    protected $collection = 'courses';
}
