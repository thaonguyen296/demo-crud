<?php


namespace App\Services;


use App\Repositories\PostRepository;

class PostService
{
    /**
     * @var $postRepository
     */
    var $postRepository;

    /**
     * PostService constructor.
     * @param PostRepository $postRepository
     */
    public function _construct(PostRepository $postRepository){
        $this->postRepository = $postRepository;
    }

    public function getAllPost(){
        return $this->postRepository->getAllPost();
    }

    public function insertPost($input)
    {
        return  $this->postRepository->insertPost($input);
    }

    public function findPost($id)
    {
        return  $this->postRepository->findPost($id);
    }

    public function Edit($id)
    {
        $post = $this->postRepository->findPost($id);
    }

    public function savePost( $input)
    {
        return $this->postRepository->save($input);
    }

    public function Delete($id)
    {
        $post= $this->postRepository->findPost($id);
        return $post->delete();
    }
}
