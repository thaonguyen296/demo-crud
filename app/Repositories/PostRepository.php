<?php


namespace App\Repositories;


use App\Models\Post;


class PostRepository
{
    public function getAllPost(){
        return Post::orderBy('id')->get();
    }
    public function insertPost( $input)
    {
        $validated = $input->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
        $post = new Post();
        $post ->title  =$input ->title;
        $post ->body  =$input ->body;
        return  $post ->save();
    }

    public function findPost($id)
    {
        return  Post::find($id);
    }
    public function save($input)
    {
        $post=Post::find($input->id);
        $post ->title  =$input ->title;
        $post ->body  =$input ->body;
        $post ->save();
    }
}

