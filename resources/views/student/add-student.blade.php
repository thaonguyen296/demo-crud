<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Student</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
<section style="padding-top:60px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header" style="font-size: 40px;">
                        Add Student
                    </div>
                    <div class="card-body">
                        {{--                        @if(Session::has('course_created'))--}}
                        {{--                            <div class="alert alert-success" role="alert">--}}
                        {{--                                {{Session::get('course_created')}}--}}
                        {{--                            </div>--}}
                        {{--                        @endif--}}
                        <form method="POST" action="{{route('student.create')}}">
                            @csrf
                            <div class="form-group">
                                <label for="studname">Student Name</label>
                                <input type="text" name="studname" class="form-control" placeholder="Enter Student Name">
                            </div>
                            <div class="form-group">
                                <label for="gender" style="padding-top: 10px;">Gender</label>
                                <input type="text" name="gender" class="form-control" placeholder="Enter Gender">
{{--                                <select id="gender">--}}
{{--                                    <option value="male">Male</option>--}}
{{--                                    <option value="female">Female</option>--}}
{{--                                </select>--}}
                            </div>
                            <div class="form-group">
                                <label for="age" style="padding-top: 10px;">Age</label>
                                <input type="text" name="age" class="form-control" placeholder="Enter Age">
                            </div>
                            <button type="submit" class="btn btn-success" style="margin-top: 10px;">Add Student</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
</body>
</html>
