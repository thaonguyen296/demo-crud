
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>All Course</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
<section style="padding-top:60px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" style="font-size: 40px;">
                        All Post <a href="/course/add-course" class="btn btn-success">Add New Course</a>
                    </div>
                    <div class="card-body">
                        @if(Session::has('course_deleted'))
                            <div class="alert alert-success" role="alert">
                                {{Session::get('course_deleted')}}
                            </div>
                        @endif
                        <table class="table table-striped">
                            <thread>
                                <tr>
                                    <th>#</th>
                                    <th>Course Name</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thread>
                            <tbody>
                            @php($i=1)
                            @foreach($courses as $course)
                                <tr>
                                    <td scope="row">{{$i++}}</td>
                                    <td>{{$course->course_name}}</td>
                                    <td>{{$course->price}}</td>
                                    <td>
                                        <a href="/course/edit-course/{{$course->id}}" class="btn btn-success">Edit</a>
                                        <a href="/course/delete-course/{{$course->id}}" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
</body>
</html>
